<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks = 0");
        DB::table('statuses')->truncate();
        DB::statement("SET foreign_key_checks = 1");
        $i = 1;
        while ($i < 200) {

            DB::table('statuses')->insert([
                'user_id' => rand(1,50),
                'body' => str_random(50),
                'created_at' => Carbon::now(),
            ]);
            $i += 1;
        }
    }
}
