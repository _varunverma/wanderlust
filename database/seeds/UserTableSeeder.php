<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks = 0");
        DB::table('users')->truncate();
        DB::statement("SET foreign_key_checks = 1");
        $i = 1;
        while ($i < 51) {

            DB::table('users')->insert([
                'firstname' => str_random(10),
                'lastname' => str_random(10),
                'username' => str_random(10),
                'email' => str_random(10) . '@gmail.com',
                'password' => bcrypt('secret'),
                'gender' => 'm',
            ]);
            $i += 1;
        }
    }
}
