<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Wanderlust</title>
    <link href="/assets/css/main.css" rel="stylesheet">
    <link href="/assets/css/bootstrap.css" rel="stylesheet">
    <link href="/assets/css/landing-page.css" rel="stylesheet">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
    <link href="/assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

</head>
<body>
    @include('layouts.partials.nav')
    <div class="jumbotron">
        <div class="container">
            @yield('jumbotron')
        </div>
    </div>
    <div class="container-fluid">
        @yield('content')
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/map.js"></script>

</body>
</html>