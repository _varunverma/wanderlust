@extends('layouts.default')
@section('jumbotron')
    <h1>Wanderlist</h1>
    @if($user->id == Auth::user()->id)
        <p>Search the places you wanted to go and add them to your wanderlist.</p>
        <p>You will get amazing things here.</p>
    @else
        <p><a class="btn btn-default btn-lg" href="/" role="button">Create Wanderlist &raquo;</a></p>
    @endif

@stop
@section('content')
    @if($user->id == Auth::user()->id)
        @include('flashmessages.message')
        @include('errors.list')
        <div class="row">
            <form class="form-group" action="{{ route('wanderlist_add_path') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-md-6">
                    <input id="searchTextField" name="place" type="text" class="form-control" placeholder="Search">
                </div>
                    <input type="submit" value="Add" class="btn btn-primary">
            </form>
        </div>
    @endif
    @if($places->count())
        @foreach($places as $place)
            <div class="row">
                <div class="col-md-10">
                    <article class="media status-media">
                        <div class="media-body">
                            <h4><a href="#">{{ $place->place }}</a></h4>
                        </div>
                    </article>
                </div>
                @if($user->id == Auth::user()->id)
                    <div class="col-md-2">
                        <article class="media wanderlist-media">
                            <div class="media-body">
                                <form {{--action="{{ Route('wanderlist_delete_path', [$user->username]) }}" method="POST"--}}>
                                    {{--<input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">--}}
                                    <input type="submit" align="center" value="Delete" class="btn btn-danger">
                                </form>
                            </div>
                        </article>
                    </div>
                @endif
            </div>

        @endforeach
    @else
        <p>Empty Wanderlist</p>
    @endif


@stop