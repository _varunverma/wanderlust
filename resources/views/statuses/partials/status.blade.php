<article class="media status-media">
    <div class="pull-left">
        <img class="media-object" src="//www.gravatar.com/avatar/{{ md5($user->email) }}?s=30}" alt="{{ $user->username }}">
    </div>
    <div class="media-body">
        <h4 class="media-heading">{{ $user->username }}</h4>
        <p>{{ $status->created_at->diffForHumans() }}</p>
        {{ $status->body }}
    </div>
</article>