@if($statuses->count())
    @foreach($statuses as $status)
        @include('statuses.partials.status')
    @endforeach
@else
    <p>This is user hasn't yet posted any status</p>
@endif