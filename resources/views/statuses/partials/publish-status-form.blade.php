<form role="form" method="POST" action="{{ Route('statuses_path') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <textarea class="form-control simplebox" name="body" rows="3" placeholder="What's on your mind"></textarea><br />
    <div class="status-post-submit">
        <button type="submit" class="btn btn-default btn-xs btn-right">
            Post Status
        </button>
    </div>
</form>