@extends('layouts.default')
@section('jumbotron')
    <h1>Profile</h1>
@stop
@section('content')

    <div class="row">
        @include('flashmessages.message')
        @include('errors.list')
        <div class="col-md-4">
            <div class="media">

                <div class="pull-left">
                    <img class="nav-gravatar" src="//www.gravatar.com/avatar/{{ md5($user->email) }}}" alt="{{ $user->username }}">
                </div>

                <div class="media-body">
                    <h1 class="media-heading">{{ $user->username }}</h1>

                    <ul class="list-inline text-muted">
                        <li>{{ $statuscount = $statuses->count() }} {{ str_plural('Status', $statuscount) }}</li>
                        <li>{{ $followercount = $followers->count() }} {{ str_plural('Follower', $followercount) }}</li>
                    </ul>

                    @foreach($followers as $follower)
                        <img class="nav-gravatar" src="//www.gravatar.com/avatar/{{ md5($follower->email) }}?s=30" alt="{{ $follower->username }}">
                    @endforeach
                </div>
                    <a class="btn btn-primary" href="{{ route('profile_path', $user->username) }}/wanderlist">Wanderlist</a>

            </div>

        </div>
        <div class="col-md-8">
            @unless($user->id == Auth::user()->id)
                @include('users.partials.follow-form')
            @endif
            @if(Auth::check())
                @if($user->id == Auth::user()->id)
                    @include('statuses.partials.publish-status-form')
                @endif
            @endif

            @include('statuses.partials.statuses')
        </div>
    </div>

@stop