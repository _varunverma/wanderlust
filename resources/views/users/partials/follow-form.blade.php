
@if ($f == true)
    <form action="{{ Route('follow_path', [$user->id]) }}" method="POST">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <button type="submit" class="btn btn-danger">UnFollow {{ $user->username }}</button>
    </form>
@else
    <form action="{{ Route('follows_path') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="userId" value="{{ $user->id }}">
        <button type="submit" class="btn btn-primary">Follow {{ $user->username }}</button>
    </form>
@endif