@extends('layouts.default')
@section('jumbotron')
    <h1>Users</h1>
@stop
@section('content')
    @foreach($users->chunk(4) as $userSet)
        <div class="row users">
        @foreach($userSet as $user)
            <div class="col-md-3 user-block">
                <a href="{{ Route('profile_path', $user->username) }}">
                    <img class="img-circle media-object avatar" src="//www.gravatar.com/avatar/{{ md5($user->email) }}?s=70}" alt="{{ $user->username }}">
                </a>
                <h4 class="user-block-username">
                    <a href="{{ Route('profile_path', $user->username) }}">
                        {{ $user->username }}
                    </a>
                </h4>
                <hr />
            </div>
        @endforeach
        </div>
    @endforeach
    {!! $users->render() !!}
@stop