@if (Session::has('flash_message'))
       <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Message</strong> {{ Session::get('flash_message') }}
    </div>
@endif