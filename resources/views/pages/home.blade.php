@extends('layouts.default')
@section('jumbotron')
      <h1>Welcome to Wanderlust</h1>
      <p>A place for Travellers</p>
      @if (Auth::guest())
         <p><a class="btn btn-default btn-lg" href="register" role="button">Sign Up &raquo;</a></p>
      @else
         <p><a class="btn btn-default btn-lg" href="{{ route('profile_path', Auth::user()->username) }}/wanderlist" role="button">Create Wanderlist &raquo;</a></p>
      @endif

   </div>
@stop
@section('content')
   @include('flashmessages.message')

@stop