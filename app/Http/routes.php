<?php

/*Event::listen('illuminate.query', function($query)
{
    var_dump($query);
});*/

// Pages routes...
Route::get('/', 'PagesController@home');

/**
 * Authentication System
 */

// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');


// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@store');
Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'Auth\AuthController@confirm'
]);
// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
/**
 * Users
 */

// Show all users
Route::get('users', [
    'as' => 'users_path',
    'uses' => 'UserController@showallusers'
]);

// News feed
Route::get('statuses', [
    'middleware' => 'auth',
    'uses' => 'StatusController@index'
]);

// Post a status
Route::post('statuses', [
    'as' => 'statuses_path',
    'middleware' => 'auth',
    'uses' => 'StatusController@store'
]);

// The profile of a single user
Route::get('@{username}', [
   'as' => 'profile_path',
   'uses' => 'UserController@showuser'
]);
/**
 * Wanderlist of user
 */

Route::get('@{username}/wanderlist', [
    'as' => 'wanderlist_path',
    'uses' => 'WanderlistController@index'
]);
Route::post('@{username}/wanderlist', [
    'as' => 'wanderlist_add_path',
    'uses' => 'WanderlistController@store'
]);

Route::delete('@{username}/wanderlist', [
    'as' => 'wanderlist_delete_path',
    'uses' => 'WanderlistController@destroy'
]);

 /**
  * Follows
  */


// Follow a user
Route::post('follows', [
    'as' => 'follows_path',
    'uses' => 'FollowsController@store'
]);

// Unfollow a user
Route::delete('follow/{id}', [
    'as' => 'follow_path',
    'uses' => 'FollowsController@destroy'
]);


// Places

Route::get('place', [
    'as' => 'place_path',
    'uses' => 'PlacesController@index'
]);
