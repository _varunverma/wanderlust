<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class PlacesController extends Controller
{
    public function index()
    {
        $placename = Input::get('search');
        return view('places.index')->with('placename', $placename);
    }
}
