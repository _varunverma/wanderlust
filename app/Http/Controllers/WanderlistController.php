<?php

namespace App\Http\Controllers;

use App\User;
use App\Wanderlist;
use Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Input;
use Redirect;

class WanderlistController extends Controller
{
    public function index($username)
    {
        $user = DB::table('users')->where('username', $username)->first();
        $places = User::find($user->id)->wanderlist()->latest()->get();

        return view('wanderlist.index')->with('user', $user)->with('places', $places);
    }

    public function store()
    {
        $rules = [
            'place' => 'required|min:1',
        ];

        // Input from Form
        $input = Input::only(
            'place'
        );
        // Getting the details of Authenticated user
        $user = Auth::user();

        // Validator for form
        $validator = Validator::make($input, $rules);

        // If Validator passes then store the comment
        if ($validator->passes()) {
            $status = new Wanderlist;
            $status->user_id = $user->id;
            $status->place = Input::get('place');
            $status->save();

            return Redirect::back()->withFlashMessage('Place Added to your wanderlist');

        } else {
            return Redirect::back()->withFlashMessage('The following errors occurred')->withErrors($validator)->withInput();
        }
    }

    /**
     * @param $username
     * @return mixed
     */
    public function destroy($username)
    {
        $user = Auth::user();

        $flight = Wanderlist::find(12);

        $flight->delete();

        return Redirect::back();
    }
}
