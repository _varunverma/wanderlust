<?php

namespace App\Http\Controllers;

use App\Follows;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class FollowsController extends Controller
{
    /**
     * Follow a user
     */
    public function store()
    {
        // Id of user to follow
        $followedId = Input::get('userId');

        // id of the Auth user
        $follower = Auth::user();

        // Storing the row into the follows table
        $follower->followings()->attach(['followed_id' => $followedId]);

        return Redirect::back()->withFlashMessage('You are now Following this user');

    }

    /**
     * Unfollow a user
     */
    public function destroy($id)
    {
        $follower = Auth::user();

        $follower->followings()->detach($id);

        return Redirect::back()->withFlashMessage('You have now unfollowed this user');;
    }
}
