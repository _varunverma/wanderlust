<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Status;
use Input;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class StatusController extends Controller
{
    /*
     * News feed of user
     */
    public function index()
    {
        // Getting the details of Authenticated user
        $user = Auth::user();

        // Getting the user's Statuses
        $statuses = User::find($user->id)->statuses()->latest()->get();

        return view('users.profile', compact('statuses'), compact('user', $user));

    }

    /**
     * Post a status
     */
    public function store()
    {
        // Rules for Posting a status
        $rules = [
            'body' => 'required|min:1',
        ];

        // Input from Form
        $input = Input::only(
            'body'
        );
        // Getting the details of Authenticated user
        $user = Auth::user();

        // Validator for form
        $validator = Validator::make($input, $rules);

        // If Validator passes then store the comment
        if ($validator->passes()) {
            $status = new Status;
            $status->user_id = $user->id;
            $status->body = Input::get('body');
            $status->save();

            return Redirect::back()->withFlashMessage('Your Status has been updated');

        } else {
            return Redirect::back()->withFlashMessage('The following errors occurred')->withErrors($validator)->withInput();
        }
    }


}
