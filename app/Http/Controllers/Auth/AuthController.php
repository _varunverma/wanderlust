<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Input;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
   /* protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }*/

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    /*protected function create(array $data)
    {
        return User::create([
            'firstname' => $data['firstname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }*/
    protected $redirectPath = '/';
    protected $loginPath = '/login';


    /**
     * Storing the data to database
     */
    public function store()
    {
        // Rules for form
        $rules = [
            'firstname' => 'required|min:2',
            'lastname' => 'required|min:2',
            'username' => 'required|min:2|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:6',
            'gender' => 'required|min:1|in:m,f'
        ];

        // Input from Form
        $input = Input::only(
            'firstname',
            'lastname',
            'username',
            'email',
            'password',
            'password_confirmation',
            'gender'
        );

        // Validator for form
        $validator = Validator::make($input, $rules);

        // Genrating a random confirmation code for email confirmation
        $confirmation_code = str_random(30);

        // If validator passes
        if ($validator->passes()) {
            $user = new User;
            $user->firstname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->password = bcrypt(Input::get('password'));
            $user->confirmation_code = $confirmation_code;
            $user->gender = Input::get('gender');
            $user->save();

            // Sending mail to the registered user for confirmation
            Mail::send('emails.emailverification', array('firstname'=>Input::get('firstname'), 'lastname' => Input::get('lastname') , 'confirmation_code'=>$confirmation_code), function($message){
                $message->from('varun.verma.1312@gmail.com', 'Wanderlust');
                $message->to(Input::get('email'), Input::get('username'))->subject('Welcome to the Travel');
            });
            return Redirect::to('login')->withFlashMessage('Thanks for registering! Now you can login ');
        } else {
            return Redirect::to('register')->withFlashMessage('The following errors occurred')->withErrors($validator)->withInput();
        }
    }

    /*
     * When user click on the link which is send to his mail
     */
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return 'You have successfully verified your account.';

        return Redirect::to('login');
    }
}
