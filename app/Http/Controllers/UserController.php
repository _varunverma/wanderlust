<?php

namespace App\Http\Controllers;

use App\follows;
use App\Status;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function status()
    {
        return view('user.statuses.index');
    }

    /**
     * Show a users in the user table
     */
    public function showallusers()
    {
        // Grabing all the users in the users table
        $users = DB::table('users')
            ->distinct()
            ->orderBy('username', 'asc')
            ->Paginate(16);

        // returning a view to it
        return view('users.index', compact('users', $users));
    }

    /**
     * The profile of a single user
     */
    public function showuser($username)
    {
        // Grabbing the user's details by username
        $user = DB::table('users')->where('username', $username)->first();
        // Grabbing the user's all statuses
        $statuses = User::find($user->id)->statuses()->latest()->get();


        // Grabbing follow table details
        $followtable = Follows::all();
        $followers = User::find($user->id)->followers()->get();
        // Checking if follow table is empty
        if(Auth::user()->followings->count()) {

            foreach (Auth::user()->followings as $follow) {
                // checking the auth user alreday followed user or not
                if ($follow->pivot->followed_id == $user->id && $follow->pivot->follower_id == Auth::user()->id) {
                    $f = true;
                    break;
                } else {
                    $f = false;
                }
            }
        }
        else {
            $f = false;
        }

        // returning a view
        return view('users.profile')
            ->with('user', $user)
            ->with('statuses', $statuses)
            ->with('f', $f)
            ->with('followers', $followers);
    }
}
