<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wanderlist extends Model
{
    /**
     * A Wanderlist belongs to a user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
